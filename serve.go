package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/urfave/cli/v2"
)

// ServeFlags returns a list of CLI flags for the serve command
func ServeFlags() []cli.Flag {
	return []cli.Flag{}
}

// Serve wraps the underlying serve function to expose as subcommand
func Serve() *cli.Command {
	return &cli.Command{
		Name:   "serve",
		Flags:  ServeFlags(),
		Action: serve,
	}
}

func validateContent(content []byte) string {
	// TODO: use content-length
	if len(content) < 1 || 200000 < len(content) {
		return "Invalid length"
	}

	return ""
}

func parseContent(r *http.Request) (string, []byte, error) {
	file, header, err := r.FormFile("file")
	if err != nil {
		return "", nil, err
	}

	b, err := io.ReadAll(file)
	return header.Filename, b, err
}

func handleScan(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		filename, content, err := parseContent(r)
		if err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
			io.WriteString(w, "{\"error\": \"Unprocessable Entity\"}")
			return
		}
		errStr := validateContent(content)
		if errStr != "" {
			w.WriteHeader(http.StatusBadRequest)
			io.WriteString(w, "{\"error\": \""+errStr+"\"}")
			return
		}

		dpath, _ := os.MkdirTemp(os.TempDir(), "")
		file, _ := os.CreateTemp(dpath, "*"+filename)
		defer os.RemoveAll(dpath)

		file.Write(content)

		out, err := analyze(&cli.Context{}, dpath)
		if err != nil {
			log.Fatalf("failed to analyze: %v\n", err)
		}

		report, _ := convert(out, "")

		log.Printf("Found %v results\n", len(report.Vulnerabilities))

		response, _ := json.Marshal(report)
		io.WriteString(w, string(response))
	default:
		http.NotFound(w, r)
	}
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Fprintf(w, "OK\n")
	default:
		http.NotFound(w, r)
	}
}

func serve(_ *cli.Context) error {
	http.HandleFunc("/scan", handleScan)
	http.HandleFunc("/", handleRoot)

	port := os.Getenv("PORT")
	if port == "" {
		port = "5555"
	}
	addr := ":" + port

	log.Println("Listening on", addr)

	log.Fatal(http.ListenAndServe(addr, nil))

	return nil
}
