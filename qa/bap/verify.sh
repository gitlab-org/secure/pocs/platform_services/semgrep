#!/usr/bin/env bash

ALLOWED_FAILURE=0
ALLOWED_TIMEDIFF=5m
ALLOWED_VULNDIFF=100
REPORT=./scan-reports/gl-bap-analysis-report.json 

export DOWNSTREAM_PIPELINE_ID=$(curl --retry 3 --retry-delay 3 --fail -sS --header "PRIVATE-TOKEN: ${SELF_RO_TOKEN}" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/bridges" | jq '.[] | .downstream_pipeline | select (.web_url | startswith("https://gitlab.com/gitlab-org/secure/tools/bap/")) | .id' | tr -d \")
export JOB_WITH_ARTIFACTS_ID=$(curl --retry 3 --retry-delay 3 --fail -sS --header "PRIVATE-TOKEN: ${BAP_RO_TOKEN}" "https://gitlab.com/api/v4/projects/${DOWNSTREAM_PROJECT_ID}/pipelines/${DOWNSTREAM_PIPELINE_ID}/jobs" | jq '.[] | select(.name=="run-diff") | .id' | tr -d \")
curl --retry 3 --retry-delay 3 --fail -sS --location --output artifacts.zip --location --header "PRIVATE-TOKEN: ${BAP_RO_TOKEN}" "https://gitlab.com/api/v4/projects/${DOWNSTREAM_PROJECT_ID}/jobs/${JOB_WITH_ARTIFACTS_ID}/artifacts"
unzip -n -qq artifacts.zip

go run qa/bap/verify.go -report $REPORT -fail $ALLOWED_FAILURE -timediff $ALLOWED_TIMEDIFF -vulndiff $ALLOWED_VULNDIFF